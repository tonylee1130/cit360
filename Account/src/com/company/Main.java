package com.company;
import java.util.Map;
import java.util.TreeMap;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class Main {

    Main() {
        try {
            readFile();
        }
        catch(IOException ex) {
            System.out.println(ex.getMessage() + " Error");
        }
    }

    TreeMap<String, Account> treeMap = new TreeMap<>();

    private void readFile() throws IOException {

        BufferedReader br = null;
        FileReader fr = new FileReader("rawData.csv");
        try {
            br = new BufferedReader(fr);

            // Read each line from the file and add each line to the ArrayList.
            String line;
            while ((line = br.readLine()) != null) {
                String[] str = line.split(",");
                if (!treeMap.containsKey(str[0])){
                    Account act = new Account(str[0], str[1], str[2], str[3]);
                    treeMap.put(str[0], act);
                }
            }
            for (Map.Entry<String, Account> entry : treeMap.entrySet()){
                System.out.println(entry.getValue().toString());
            }
        }
        finally {
            // Close the outermost reader that was successfully opened.
            if (br != null) {
                br.close();
            }
            else {
                fr.close();
            }
        }
    }

    public static void main(String[] args) {
	// write your code here
        new Main();
    }
}


