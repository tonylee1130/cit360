package com.company;

public class Account {
    private String accountNumber;
    private String accountType;
    private String givenName;
    private String familyName;

    Account (String an, String at, String gn, String fn) {

        accountNumber = an;
        accountType = at;
        givenName = gn;
        familyName = fn;
    }

    public String toString(){
        return accountNumber + ", " + accountType + ", " + givenName + ", " + familyName;
    }
}
