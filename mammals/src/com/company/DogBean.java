package com.company;
import com.company.MammalBean;
import org.jboss.arquillian.test.spi.annotation.TestScoped;

public class DogBean extends MammalBean {

    private String breed;
    private String name;

    DogBean(String b, String n, int l, String c, double h) {
        super(l,c,h);
        breed = b;
        name = n;
    }

    private void setBreed(String b){
        breed = b;
    }

    private String getBreed(){
        return breed;
    }

    private void setName(String n){
        name = n;
    }

    private String getName(){
        return name;
    }

    @Override
    public String toString(){
        return super.toString() + " breed=" + breed + " name=" + name;
    }
}


