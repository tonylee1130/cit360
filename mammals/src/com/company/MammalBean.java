package com.company;

public class MammalBean {
    private int legCount;
    private String color;
    private double height;

    MammalBean(int l, String c, double h) {

        legCount = l;
        color = c;
        height = h;

    }

    private void setLegCount(int l){
        legCount = l;
    }

    public int getLegCount(){
        return legCount;
    }

    private void setColor(String c){
        color = c;
    }

    public String getColor(){
        return color;
    }

    private void setHeight(double h){
        height = h;
    }

    public double getHeight(){
        return height;
    }

    @Override
    public String toString(){
        return "legCount=" + legCount + " color=" + color + " height=" + height;
    }

}
