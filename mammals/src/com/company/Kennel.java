package com.company;
import com.company.DogBean;

public class Kennel {

    private DogBean[] buildDogs(){
        DogBean[] dogBean = new DogBean[5];

        dogBean[0] = new DogBean("Beagle", "Happy", 4, "black", 23);
        dogBean[1] = new DogBean("Pitbull", "Grumpy", 3, "brown", 10);
        dogBean[2] = new DogBean("Husky", "Sad", 2, "blue", 15);
        dogBean[3] = new DogBean("Golden Retriever", "Sleepy", 1, "Golden", 30);
        dogBean[4] = new DogBean("Lab", "Lazy", 0, "Orange", 20);

        return dogBean;
    }

    private void displayDogs(DogBean[] buildDogs){
        for(int i =0; i < 5; i++){
            System.out.println(buildDogs[i].toString());
        }
    }

    public static void main(String[] args) {

        Kennel kennel = new Kennel();

        DogBean[] allDogs = kennel.buildDogs();
        kennel.displayDogs(allDogs);
    }

}
