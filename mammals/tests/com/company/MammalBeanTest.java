package com.company;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class MammalBeanTest {

    @Test
    public void mammalBeanTest1(){

        try {
            MammalBean m1 = new MammalBean(3, "blue", 12);
            MammalBean m2 = new MammalBean(2, "purple", 10);
            MammalBean m3 = new MammalBean(4, "black", 19);
            MammalBean m4 = new MammalBean(4, "brown", 18);

            DogBean d1 = new DogBean("beagle", "Charlie", 2, "brown", 10);
            DogBean d2 = new DogBean("boxer", "Brown", 4, "blue", 15);
            DogBean d3 = new DogBean("poodle", "Snoopy", 4, "black", 13);
            DogBean d4 = new DogBean("german shepard", "Scooby", 3, "white", 11);

            System.out.println(m1.toString());
            System.out.println(m2.toString());
            System.out.println(m3.toString());
            System.out.println(m4.toString());

            System.out.println(d1.toString());
            System.out.println(d2.toString());
            System.out.println(d3.toString());
            System.out.println(d4.toString());

        } catch (Exception ex) {
            System.out.println(ex.getClass() + " " + ex.getMessage() + " " + ex.toString());
        }
    }

    @Test
    public void createMammal() {
        try {
            MammalBean m1 = new MammalBean(3, "blue", 12);
            MammalBean m2 = new MammalBean(2, "purple", 10);
            MammalBean m3 = new MammalBean(4, "black", 19);
            MammalBean m4 = new MammalBean(4, "brown", 18);
            Set set = new TreeSet();

            set.add(m1);
            set.add(m2);
            set.add(m3);
            set.add(m4);
            
            //Color
            assertEquals("blue", m1.getColor());
            assertEquals("purple", m2.getColor());
            assertEquals("black", m3.getColor());
            assertEquals("brown", m4.getColor());
            //legCount
            assertEquals(3, m1.getLegCount());
            assertEquals(2, m2.getLegCount());
            assertEquals(4, m3.getLegCount());
            assertEquals(4, m4.getLegCount());
            //height
            double x = 0.001;
            assertEquals(12, m1.getHeight(), x);
            assertEquals(10, m2.getHeight(), x);
            assertEquals(19, m3.getHeight(), x);
            assertEquals(18, m4.getHeight(), x);

            DogBean d1 = new DogBean("beagle", "Charlie", 2, "brown", 10);
            DogBean d2 = new DogBean("boxer", "Brown", 4, "blue", 15);
            DogBean d3 = new DogBean("poodle", "Snoopy", 4, "black", 13);
            DogBean d4 = new DogBean("german shepard", "Scooby", 3, "white", 11);


            TreeMap<String, DogBean> dogTest = new TreeMap<>();
            dogTest.put("Cody", d1);
            dogTest.put("Dogma", d2);
            dogTest.put("Hardcase", d3);
            dogTest.put("Echo", d4);

            assertEquals(true, dogTest.containsKey("Cody"));
            assertEquals(true, dogTest.containsKey("Dogma"));
            assertEquals(true, dogTest.containsKey("Hardcase"));
            assertEquals(true, dogTest.containsKey("Echo"));


            dogTest.remove("Cody");

            assertEquals(false, dogTest.containsKey("Cody"));
            assertEquals(false, dogTest.containsKey("Dogma"));
            assertEquals(false, dogTest.containsKey("Hardcase"));
            assertEquals(false, dogTest.containsKey("Echo"));


        } catch (Exception ex) {
            System.out.println(ex.getClass() + " " + ex.getMessage() + " " + ex.toString());
        }
    }
}